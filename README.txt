INSTALLATION

1. Install and enable the libraries module

2. Put six JavaScript libraries into the sites/.../libraries directory.
   In each case, go to the github page, click on Download Zip, download the
   zip file into the libraries directory (.../sites/all/libraries or
   .../sites/$sitename/libraries) and unzip it there.  The libraries are:
   https://github.com/wout/svg.js
   https://github.com/wout/svg.draggable.js
   https://github.com/wout/svg.easing.js
   https://github.com/sampumon/SVG.toDataURL
   https://github.com/gabelerner/canvg
   https://github.com/enyo/opentip
