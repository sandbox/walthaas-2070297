<?php

/**
 * @file
 * Drush commands for the snow_profile module
 */

/**
 * Implements hook_drush_command().
 */
function snow_profile_drush_command() {
  $items = array();

  // The 'sp-rm' command
  $items['sp-rm'] = array(
    'description' => 'Disable and uninstall the snow_profile, snow_profile_data and snow_profile_example modules',
  );
  return $items;
}

/**
 * Implements drush_hook_COMMAND().
 */
function drush_snow_profile_sp_rm() {
  drush_include_engine('drupal', 'environment');

  // Remove all traces of the snow_profile_example module
  if (module_exists('snow_profile_example')) {
    module_disable(array('snow_profile_example'), FALSE);
    if (module_exists('snow_profile_example')) {
      drush_log('Disable snow_profile_example failed', 'error');
    }
    else {
      drush_log('snow_profile_example disabled', 'ok');
    }
  }
  drush_module_uninstall(array('snow_profile_example'));

  // Remove all traces of the snow_profile module
  if (module_exists('snow_profile')) {
    module_disable(array('snow_profile'), FALSE);
    if (module_exists('snow_profile')) {
      drush_log('Disable snow_profile failed', 'error');
    }
    else {
      drush_log('snow_profile disabled', 'ok');
    }
  }
  drush_module_uninstall(array('snow_profile'));

  // Remove all traces of the snow_profile_data module
  if (module_exists('snow_profile_data')) {
    module_disable(array('snow_profile_data'), FALSE);
    if (module_exists('snow_profile_data')) {
      drush_log('Disable snow_profile_data failed', 'error');
    }
    else {
      drush_log('snow_profile_data disabled', 'ok');
    }
  }
  drush_module_uninstall(array('snow_profile_data'));
}
